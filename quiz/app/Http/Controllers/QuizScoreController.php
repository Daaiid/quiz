<?php

namespace App\Http\Controllers;

use App\QuizScore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuizScoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->authorizeResource(QuizScore::class, 'scores');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scores = QuizScore::all();

        return response($scores);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'quiz_id' => 'required',
            'total_score' => 'required'
        ]);

        $score = new QuizScore([
            'quiz_id' => $request->get('question_id'),
            'total_score' => $request->get('total_score')
        ]);

        $score->save();

        return response(null, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $score = QuizScore::find($id);

        return response($score);
    }

        /**
     * Returns the single quizscore corresponding to the 
     * authenticated user and the given quizId
     */
    public function showForQuiz($id)
    {
        $userId = Auth::id();
    
        $score = QuizScore::where('user_id', '=', $userId)
        ->where('quiz_id', '=', $id)->first();

        return response($score);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'quiz_id' => 'required',
            'total_score' => 'required|integer'
        ]);

        if($score->total_score <= $request->total_score)
        {
            $score->fill($request->total_score);
            $score->save();
        }
        else
        {
            return response(null, 404);
        }
        
        return response(null, 204);
    }

}
