<?php

namespace App\Http\Controllers;

use App\QuizScore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class RankingController extends Controller
{
    /**
     * Gets the top 20 ranking users (ordered).
     */
    public function getTopTwenty()
    {
        // Writing this in SQL seemed easier to me and it isn't vulnerable
        // to SQL Injection, so what's the harm
        $rankingsTop20 = DB::select(DB::raw(
            "SELECT user_id, users.name AS username, SUM(total_score) AS user_score, 
                RANK() OVER (ORDER BY user_score DESC) AS user_rank    
            FROM quiz_scores
            LEFT JOIN users ON user_id = users.id
            GROUP BY user_id, username 
            ORDER By user_score DESC
            LIMIT 20"));

        return response($rankingsTop20);
    }

    /**
     * Gets the session user's score and rank
     */
    public function getUserRank()
    {
        if (Auth::check()) 
        {
            $userId = Auth::id();

            $rank = DB::select(DB::raw(
                "SELECT user_id, users.name AS username, SUM(total_score) AS user_score, 
                    RANK() OVER (ORDER BY user_score DESC) AS user_rank           
                FROM quiz_scores
                LEFT JOIN users ON user_id = users.id
                WHERE user_id = $userId
                GROUP BY user_id, username"));

            return response($rank);
        }

        return response(null, 401);
    }



}
