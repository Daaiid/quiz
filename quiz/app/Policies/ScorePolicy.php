<?php

namespace App\Policies;

use App\QuizScore;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ScorePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any quiz scores.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the quiz score.
     *
     * @param  \App\User  $user
     * @param  \App\QuizScore  $quizScore
     * @return mixed
     */
    public function view(User $user, QuizScore $quizScore)
    {
        return true;
    }

    /**
     * Determine whether the user can create quiz scores.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the quiz score.
     *
     * @param  \App\User  $user
     * @param  \App\QuizScore  $quizScore
     * @return mixed
     */
    public function update(User $user, QuizScore $quizScore)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the quiz score.
     *
     * @param  \App\User  $user
     * @param  \App\QuizScore  $quizScore
     * @return mixed
     */
    public function delete(User $user, QuizScore $quizScore)
    {
        return true;
    }

    /**
     * Determine whether the user can restore the quiz score.
     *
     * @param  \App\User  $user
     * @param  \App\QuizScore  $quizScore
     * @return mixed
     */
    public function restore(User $user, QuizScore $quizScore)
    {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the quiz score.
     *
     * @param  \App\User  $user
     * @param  \App\QuizScore  $quizScore
     * @return mixed
     */
    public function forceDelete(User $user, QuizScore $quizScore)
    {
        return true;
    }
}
