<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class QuizScore extends Model
{
    protected $fillable = [
        'quiz_id',
        'total_score'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($score){
            $user = Auth::user();
            $score->user_id = $user->id;
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function quiz()
    {
        return $this->belongsTo(Quiz::class);
    }
}
