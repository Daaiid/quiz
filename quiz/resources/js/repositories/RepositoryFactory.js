import QuizRepository from './QuizRepository';
import QuestionRepository from "./QuestionRepository";
import AnswerRepository from "./AnswerRepository";
import ScoreRepository from "./ScoreRepository";
import RankingsTop20Repository from './RankingsTop20Repository';
import RankingUserRepository from './RankingUserRepository';

const repositories = {
    quizzes: QuizRepository,
    questions: QuestionRepository,
    answers: AnswerRepository,
    scores: ScoreRepository,
    rankingsTop20: RankingsTop20Repository,
    rankingUser: RankingUserRepository,
    // other repositories...
};

export const RepositoryFactory = {
    get: name => {
        const repository = repositories[name];
        return repository;
    }
};