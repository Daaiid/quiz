import Repository from 'axios';

const resource = '/rankings/top20';

export default {
    get() {
        return Repository.get(`${resource}`);
    },
}