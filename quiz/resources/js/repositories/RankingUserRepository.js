import Repository from 'axios';

const resource = '/rankings/user';

export default {
    get() {
        return Repository.get(`${resource}`);
    },
}