import Repository from 'axios';

const resource = '/scores';

export default {
    getOfQuiz(quizId){
        return Repository.get(`${resource}/${quizId}/scoreForQuiz`);
    },

    createScore(payload){
        return Repository.post(`${resource}`, payload);
    },

    updateScore(scoreId, payload){
        return Repository.patch(`${resource}/${scoreId}`, payload);
    },
        
}