# TODOs

## Said
* API für Top 20 User (Tests ausstehend)
* Punktevergabe nach Quizabschluss (Tests ausstehend)

## Florian
* Rankings (said übergibt mir eine schone geordnete liste + den aktuell eingeloggten user)
* zweiter Fragetyp

## Notes
Default Fragetyp wird mit 0 gekennzeichnet, Richtig-Falsch Fragen mit Fragetyp 1.

### Routen für Ranking Daten: 
/rankings/top20 -> gibt die top 20 benutzer inkl score und ranking zurück (bereits sortiert)
/rankings/user -> gibt das ranking des eingeloggten Benutzers zurück

### Properties der Ranking Objekte (für beide Routen identisch):
* user_id
* username
* user_score
* user_rank

### HTTP 422 auf sendQuestion
Zurzeit wird, wenn versucht wird eine Frage anzulegen, Fehlercode 422 zurückgegeben. Grund dafür ist der noch fehlende Fragetyp Parameter.