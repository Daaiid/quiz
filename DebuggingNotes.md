# Notes from Said
## Things that work as intended
* "php artisan route:list" on the terminal properly shows the correct routes of the resource controller
* Depending on the game state the correct request is made to the server: if there was no previous score, the request is a post, else it's a patch request. 
* Auth-Token is correctly set, the request contents are as they should
* The user- and quiz-specific score can be retrieved from the non resource controller specific action (scoreForQuiz), which has its own specific route definition on the api.php file

## Things that break unexpectedly
* Every request to the QuizScoreController returns a 403 error code and I have absolutely no clue why. If I remove the constructor, I can access some data, but I cannot access the session's user, which isn't the point of it at all.
* The same counts for authenticated users
* I have looked through three tutorials, that show how to do API authentication with Laravel Passport and I still couldn't get it to work.
* I'm at my wit's end, so I hope the mistake isn't something silly.

## Tried, but did not change the broken stuff
* added a policy for the controller, where every action is allowed for everyone and registered the policy in the auth service
* the error from the stacktrace does NOT relate to the problem at hand or give useful googling material